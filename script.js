"use strict";
// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і
//    додайте його до розділу <section id="content">

const btn = document.getElementById("btn-click");
const section = document.getElementById("content");
btn.onclick = function () {
  const p = document.createElement("p");
  p.textContent = "New Paragraph";
  section.appendChild(p);
};

//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//   По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name.
//  та додайте його під кнопкою.

const button = document.createElement("button");
button.setAttribute("id", "btn-input-create");
section.appendChild(button);

button.addEventListener("click", () => {
  const input = document.createElement("input");
  input.type = "text";
  input.placeholder = "Enter your name";
  input.name = "username";

  section.insertBefore(input, button);
});
